#!/usr/bin/env python3
import subprocess
import json
import re
import os
import logging
import sys
from datetime import datetime

# This script returns the monitor name and serial of the active screen.
# KDE Active screen is a screen with mouse cursor.


def get_active_screen_id():
    datetime_now = datetime.now()

    file = open("/tmp/get_active_screen.js", "w")
    script = """
    as = workspace.activeScreen;
    print(as + 1)
    """
    file.write(script)
    file.close
    del file, script

    script = "/tmp/get_active_screen.js"

    reg_script_number = subprocess.run("dbus-send --print-reply --dest=org.kde.KWin \
                            /Scripting org.kde.kwin.Scripting.loadScript \
                            string:" + script + " | awk 'END {print $2}'",
                            capture_output=True, shell=True).stdout.decode().split("\n")[0]

    subprocess.run("dbus-send --print-reply --dest=org.kde.KWin /" + reg_script_number + " org.kde.kwin.Script.run",
                    shell=True, stdout=subprocess.DEVNULL)
    subprocess.run("dbus-send --print-reply --dest=org.kde.KWin /" + reg_script_number + " org.kde.kwin.Script.stop",
                    shell=True, stdout=subprocess.DEVNULL)  # unregister number

    since = str(datetime_now)

    msg = subprocess.run("journalctl _COMM=kwin_wayland -o cat --since \"" + since + "\"",
                            capture_output=True, shell=True).stdout.decode().rstrip().split("\n")
    msg = [el.lstrip("js: ") for el in msg]
    if len(msg) != 1:
        exit(1)

    return int(msg[0])


active_screen_id = get_active_screen_id()

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

# process_str = "kscreen-console json"
# json_body = "\n".join(subprocess.run(process_str, shell=True, stdout=subprocess.PIPE).stdout.decode("utf-8").split("\n")[0:])
json_body = subprocess.run(["kscreen-console", "json"], capture_output=True, env=dict(os.environ, QT_FORCE_STDERR_LOGGING="1")).stdout #.decode("utf-8")
# json_body_str = json_body.decode("utf-8")

file = open("file.txt", "wb")
file.write(json_body)
file.close
del file

with open("file.txt") as f:
   json_body2 = f.read()
del f

# Some shit is happening depending on environment (I think). When no env, it is normal, but when is, it has two lines before. So try to strip them only when needed.
if json_body2[0] != "{":
    json_body3 = "\n".join(json_body2.split('\n')[2:])
    logging.debug("stripped lines")
else:
    json_body3 = json_body2
    logging.debug("did not need to strip lines")

json_body = json_body3
outputs = json.loads(json_body)["outputs"]
outputs = [ output for output in outputs if output["connected"] ] #  Drop disconnected outputs

active_screen_full_name = ""
for output in outputs:
    if output["id"] != active_screen_id:
        continue
    else:
        active_screen_full_name = output["name"]
        break
del output

# The "kscreen-console outputs" prints monitor model names and serials, but "kscreen-console json" does not.
# TODO: file bug report for this.
# So currently will print the output name, and bash will do the rest.
# print(target_output)


process_str = """kscreen-console outputs | grep -E "Name|Serial" | grep \"""" + active_screen_full_name + """\" -A2"""
p = subprocess.run(process_str, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=dict(os.environ, QT_FORCE_STDERR_LOGGING="1"))
out = p.stdout.decode("utf-8")
out = out.split('\n')

mon_model = re.search('"DELL (.+?)"', out[1]).group(1)
mon_serial = re.search('"(.+?)"', out[2]).group(1)
print(mon_model)
print(mon_serial)
