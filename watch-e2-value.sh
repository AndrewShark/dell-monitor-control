#!/bin/bash

e2_val=$(sudo ddcutil getvcp e2 | sed "s/^.*sl=0x\(.*\)/\1/")

#P2415Q
case $e2_val in
00) preset_name=Стандарт ;;
01) preset_name=Мультимедиа ;;
02) preset_name=Кино ;;
04) preset_name=Игра ;;
0e) preset_name=Теплый ;;
12) preset_name=Холодный ;;
14) preset_name=Обычный ;;
19) preset_name=Бумага ;;
*)  echo "error - unknown preset is set?"; exit 1 ;;
esac

echo "$e2_val ($preset_name)"
