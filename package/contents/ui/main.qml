import QtQuick 2.4
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.plasma.plasmoid 2.0
import org.kde.kquickcontrolsaddons 2.0
import QtQuick.Window 2.2
import QtQml 2.15


//import org.kde.private.kscreen 1.0

Item {
    id: root
    Layout.fillWidth: true
    
    readonly property string kcmName: "kcm_kscreen"
    readonly property bool kcmAllowed: KCMShell.authorize(kcmName + ".desktop").length > 0
    property string selectedSN: "unknown"
    
    PlasmaCore.DataSource {
        id: executable
        engine: "executable"
        connectedSources: []
        onNewData: disconnectSource(sourceName) // cmd finished
        
        function exec(cmd) {
            connectSource(cmd)
        }
    }
    
    function getKscreenOutputs() {
        executable.exec("kscreen-console outputs | grep -E \"Name|Serial\"")
    }
    
    Plasmoid.fullRepresentation: Item {
        Layout.preferredWidth: 540 * PlasmaCore.Units.devicePixelRatio
        Layout.preferredHeight: 280 * PlasmaCore.Units.devicePixelRatio
        Layout.fillWidth: true

            ColumnLayout {
                anchors.left: parent.left
                anchors.right: parent.right

                GridLayout {
                    id: grid
                    columns: 3
                    width: parent.width

                    PlasmaComponents3.Label {
                        text: i18n("Monitor")+":"
                    }
                    PlasmaComponents3.ComboBox {
                        id: monSelectBox
                        Layout.columnSpan : 2
                        textRole: "text"
                        valueRole: "value"
    //                     enabled: false
                        Layout.fillWidth: true
                        // model: [
                        //     //{ value: "a", text: "Dell P2217H" },
                        //     //{ value: "b", text: "Dell P2415Q" },
                        //     { value: "c", text: "Dell P2217H2" },
                        //     { text: Screen.model }
                        // ]

                        model: {
                            var names = []
                            for (var i = 0; i < Qt.application.screens.length; ++i)
                            {
                                names.push({text: Qt.application.screens[i].model})
                            }
                            return names
                        }

                        onActivated: {
                            console.log("HH World")
                            console.log("The current screen is: " + Screen.model)
                            console.log("The current screen sn is: " + Screen.model.split('/')[1])
                            console.log("cur txt: " + monSelectBox.currentText)
                            selectedSN = monSelectBox.currentText.split('/')[1]
                            console.log("prop txt: " + selectedSN)
                            console.log("GG World")

                        }

                        Component.onCompleted: {
                            console.log("cur ind: " + currentIndex)
                            console.log("cur ind txt: " + textAt(currentIndex))
                            console.log("cur screen: " + Screen.model)
                            console.log("ind of text cur screen: " + find(Screen.model))
                            currentIndex = find(Screen.model)
                            selectedSN = monSelectBox.currentText.split('/')[1]
                        }

                    }
                    PlasmaComponents3.Label {
                        text: i18n("Resolution")+":"
                    }
                    PlasmaComponents3.Label {
                        text:  Screen.width+"x"+Screen.height
                        Layout.fillWidth: true
                    }
                    PlasmaComponents3.Button {
                        text: i18n("Change")
                        Layout.alignment: Qt.AlignRight
                        visible: kcmAllowed

                        onClicked: {
                            KCMShell.open(kcmName);
                        }
                    }
                }

                PlasmaComponents3.MenuSeparator{
                    Layout.fillWidth: true
                }

                GridLayout {
                    columns: 2
                    PlasmaComponents3.RadioButton {
                        text: i18n("Auto Mode")
                        checked: true
                        autoExclusive: true
                    }
                    PlasmaComponents3.Label {
                        text: i18n("Configure")
                        Layout.alignment: Qt.AlignRight
                        Layout.fillWidth: true
                    }
                    PlasmaComponents3.RadioButton {
                        text: i18n("Manual Mode")
                        autoExclusive: true
                    }
                    PlasmaComponents3.ComboBox {
                        textRole: "text"
                        valueRole: "value"
                        model: [
                            { value: "a", text: i18n("Standard") },
                            { value: "b", text: i18n("Paper") },
                            { value: "c", text: i18n("Game") },
                        ]

                        onActivated: {
                            executable.exec("python /home/andrew/Development/monitor-commander/switch_preset.py " + currentText)
                        }
                    }
                }

                PlasmaComponents3.MenuSeparator{
                    Layout.fillWidth: true
                }

                GridLayout {
                    Layout.fillWidth: true
                    columns: 3
                    PlasmaComponents3.Label {
                        text: i18n("Brightness")+":"
                    }
                    PlasmaComponents3.Slider {
                        id: slider
                        Layout.fillWidth: true
                        from: 0
                        to: 100
                        value: 50
                        stepSize: 1

                        onValueChanged: {
                            console.log("value changed: " + value)
                            executable.exec("ddcutil --sn=" + selectedSN + " setvcp 10 " + value)
                        }
                    }
                    PlasmaComponents3.Label {
                        id: sliderValueLabel
                        Layout.alignment: Qt.AlignRight
                        function formatText(value) {
                            return i18n("%1%", value)
                        }
                        text: formatText(slider.value)

                        TextMetrics {
                            id: textMetrics
                            font.family: sliderValueLabel.font.family
                            font.pointSize: sliderValueLabel.font.pointSize
                            text: sliderValueLabel.formatText(slider.to)
                        }

                        Layout.minimumWidth: 40 // Fixme
                        // Fixme: do not hardcode the value. I hardcoded it to allow it to be more than 3 characters width. Because
                        // if the value is textMetrics.width, then when going from 99 to 100, it floats the whole grid.
                    }
                    PlasmaComponents3.Label {
                        text: i18n("Contrast")+":"
                    }
                    PlasmaComponents3.Slider {
                        id: slider2
                        Layout.fillWidth: true
                        from: 0
                        to: 100
                        value: 50
                        stepSize: 1
                        onValueChanged: {
                            console.log("value changed: " + value)
                            executable.exec("ddcutil --sn=" + selectedSN + " setvcp 12 " + value)
                        }
                    }
                    PlasmaComponents3.Label {
                        id: sliderValueLabel2
                        Layout.alignment: Qt.AlignRight
                        function formatText(value) {
                            return i18n("%1%", value)
                        }
                        text: formatText(slider2.value)

                        TextMetrics {
                            id: textMetrics2
                            font.family: sliderValueLabel2.font.family
                            font.pointSize: sliderValueLabel2.font.pointSize
                            text: sliderValueLabel2.formatText(slider.to)
                        }
                        Layout.minimumWidth: 40 // Fixme, see above.
                    }
                }
            }
    }
    
    Component.onCompleted: {
        console.log("Hello World")
    }
}
