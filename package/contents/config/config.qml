import QtQuick 2.0
import org.kde.plasma.configuration 2.0

ConfigModel {
    //ConfigCategory {
        //name: i18n("Basic")
        //icon: "configure"
        //source: "configBasic.qml"
    //}
    ConfigCategory {
        name: i18n("Auto Mode")
        icon: "org.kde.plasma.windowlist"
        source: "configAutoMode.qml"
    }
}
