# Dell Monitor Control in Linux

I am trying to get the ability to switch Preset Modes with ddcutil under linux.
In ddm.exe there is such a command for this purpose:  
`SetNamedPreset [Movie/CAL1,etc] - changes the Preset mode*`
Also, there is a generic control command there:  
`SetControl X Y - sets hex control X to hex value Y`

I need to figure out how to achieve that SetNamedPreset command effect via standard SetControl X Y commands.

In ddcutil/ddcui I can see 0xdc (Display Mode) control and it works normally, i.e. the monitor changes its brightness and color. But it does not contains all modes that are in dell menu (I think they are just standard for all monitors, but I may be wrong). Seems like ddm uses another feature that is 0xe2. This is from capabilities scanning output:

```
Feature: 0xe2 (Manufacturer Specific)
Values:
0x00: No lookup table
0x1d: No lookup table
0x01: No lookup table
0x02: No lookup table
0x04: No lookup table
0x0e: No lookup table
0x12: No lookup table
0x14: No lookup table
```

When I switch a Preset Mode manually via physical buttons on the monitor, and then read the value of that control (0xe2) with ddcutil getvcp, I can see that it changes:

```
# ddcutil getvcp e2     
VCP code 0xe2 (Manufacturer Specific         ): mh=0x00, ml=0xff, sh=0x00, sl=0x1d
# ddcutil getvcp e2
VCP code 0xe2 (Manufacturer Specific         ): mh=0x00, ml=0xff, sh=0x00, sl=0x01
```

But I am unable to change it via ddcutil setvcp:

```
# ddcutil setvcp e2 x1d
Verification failed for feature e2
```

So need to figure out how to achieve that SetNamedPreset command effect via standard SetControl X Y commands. After I know that, I could just do the same via ddcutil in linux. 

# Sniff commands on i2c bus under Windows?
DDM installs correctly under wine. However it cannot detect any Dell monitor. In terminal there are many messages that tells that some functions are not implemented.

So I see two ways here:
## Make ddm work under wine
Then I could use the sniffing method described here: https://linuxtv.org/wiki/index.php/Bus_snooping/sniffing#i2c

To make ddc/ci windows applications work in wine, it is easier to start with open source windows applications communicating via ddc/ci, such as Monitorian - https://github.com/emoacht/Monitorian  
Need to implement stub functions to work properly.  
Then need to test if it starts and works normally.

A close source candidate for testing is softMCCS software, also from entech taiwan - https://www.entechtaiwan.com/lib/softmccs.shtm

## Make i2c sniffing tool for Windows
Then I could check commands in windows and then just repeat them in ddcutil in linux.

# Download latest Dell Display Manager
https://www.delldisplaymanager.com/ddmsetup.exe - Link to the latest version download.
On the dell.com site the version is often outdated.

# Just probe
Probably there is a way to guess what are their manufacturer specific controls.  
What is needed to do is manually setting a property by physical monitor buttons. Then
`$ sudo ddcutil probe > at_paper.txt`
then switch property, then again probe:
`$ sudo ddcutil probe > at_game.txt`
then compare files
`kompare at_paper.txt at_game.txt`
and try to understand what is what.
value that needed.

# Permission requirement

In Arch Linux you should add your uuser to the i2c group and reboot.
To auto load the module you should manually add /etc/modules-load.d/i2c-dev.conf with the line "i2c-dev". This is until https://bugs.archlinux.org/task/76233 is fixed.

# See also
https://www.dell.com/support/kbdoc/ru-ru/000060112/%d1%87%d1%82%d0%be-%d0%bf%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b0%d0%b2%d0%bb%d1%8f%d0%b5%d1%82-%d1%81%d0%be%d0%b1%d0%be%d0%b9-dell-display-manager - screenshots of dell display manager application


# Plasmoid
## Link to a dev files from standard location
You want to make a link from standard location to dev files, because every time copying is not convenient.

Creating symlink does not help for some reason:
`ln -s /home/andrey/Development/monitor-commander/package /home/andrey/.local/share/plasma/plasmoids/org.kde.plasma.monitorcommander`
Is there a bug report that symbolic links are not followed?

Solution with binding works:
`sudo mount -o bind /home/andrey/Development/monitor-commander/package /home/andrey/.local/share/plasma/plasmoids/org.kde.plasma.monitorcommander`
Be careful to not uninstall the widget, as it will remove your original files.

## Reinstall plasmoid for the non-bind method

Be carefull not to use these commands if you use bind method described above. It may erase your files in the dev directory.
```
# This script reinstalls plasmoid and replaces plasmashell.
# Replace is needed even after uninstall+install. Without it, the contents of the plasmoid is not updated.

kpackagetool5 -t Plasma/Applet --remove ~/Development/monitor-commander/package
kpackagetool5 -t Plasma/Applet --install ~/Development/monitor-commander/package
# kpackagetool5 -t Plasma/Applet --upgrade ~/Development/monitor-commander/package
plasmashell --replace & >/dev/null 2>&1
```

## KDevelop setup

When using bind method described above, we do not need to deinstall and install the widget. But we still need to relaunch plasma shell, so plasma will reload a widget on a panel.

To simplify this thing, we can create a shortcut for this. Open "External Scripts" tool view, add a new script with the following command: `plasmashell --replace & >/dev/null 2>&1`. In Shortcut set any shortcut you want, for example Ctrl + F11.

Now at the moment you want to see your new changes, save the documents, and press shortcut. And see the behavior of plasmoid.

If wanting to use plasmoid viewer (faster than waiting plasmashell replace), you can create launch configuration for plasmoid. But there is a bug when using plasmoid launcher that does not show console.log, see https://bugs.kde.org/show_bug.cgi?id=463967, so you need to use script application launcher from workaround 1.
