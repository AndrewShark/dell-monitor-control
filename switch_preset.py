#!/usr/bin/env python3

import subprocess
import configparser
import sys
import os
import logging

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
my_env = os.environ.copy()

cmd = "python /home/andrew/Development/monitor-commander/get_mon_by_mouse_pos.py"
pout = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, env=my_env).stdout
mon_model = pout.decode().split('\n')[0]
mon_serial = pout.decode().split('\n')[1]

if mon_model == "" or mon_serial == "":
    print("Something went wrong")
    cmd = """kdialog --passivepopup "Something went wrong" """
    subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)

desired_preset = sys.argv[1]

if desired_preset == "DefaultPreset":
    # cmd = """kdialog --passivepopup "We are going to default preset" """
    # subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)

    config = configparser.ConfigParser()
    config.read("/home/andrew/Development/dell-monitor-control/settings.ini")
    desired_preset = config.get(mon_serial, "DefaultPreset")
    # cmd = """kdialog --passivepopup "Choosing default preset, it is called """ + desired_preset + """ " """
    # subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)

# cmd = """kdialog --passivepopup " """ + desired_preset + """ " """
# subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)

mon_presets = {
    "P2217H": {
        # Standard
        'ComfortView': "0xf0 0x0c",
        # Multimedia
        # Movie
        'Game': "0xdc 0x05",
        # Warm
        # Cool
        # Custom Color
    },
    "P2415Q": {
        # Standard
        # Multimedia
        # Movie
        'Game': "0xdc 0x05",
        'Paper': "0xf0 0x08",
        # Warm
        # Cool
        # Custom Color
    },
}

# ddcutil_par_str = mon_presets["P2415Q"]["Game"]
ddcutil_par_str = mon_presets[mon_model][desired_preset]

print("Switching str is " + ddcutil_par_str)
# cmd = """kdialog --passivepopup "Switching str is """ + ddcutil_par_str + """ " """
# subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)

process_str = """ddcutil --sn=""" + mon_serial + """ setvcp """ + ddcutil_par_str
subprocess.run(process_str, shell=True, stdout=subprocess.PIPE).stdout
print("goodbye")
